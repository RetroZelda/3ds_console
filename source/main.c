#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <3ds.h>

#include "Console.h"


int main()
{
	// Initialize services
	srvInit();
	aptInit();
	hidInit(NULL);
	gfxInit();
	//gfxSet3D(true); // uncomment if using stereoscopic 3D


    // start console
    //OpenConsole(GFX_TOP);
    OpenConsole(GFX_BOTTOM);
    printf("Opened\tConsole!");

    int nTestCount = 0;
    int frame = 0;
	// Main loop
	while (aptMainLoop())
	{
		gspWaitForVBlank();
		hidScanInput();

		// clear the screens
        u8* TopFrameBuffer = gfxGetFramebuffer(GFX_TOP, GFX_LEFT, NULL, NULL);
        u8* BottomFrameBuffer = gfxGetFramebuffer(GFX_BOTTOM, GFX_LEFT, NULL, NULL);

        memset(TopFrameBuffer, 0, 240*400*3);
        memset(BottomFrameBuffer, 0, 240*320*3);

		u32 kDown = hidKeysDown();
		if (kDown & KEY_START)
			break; // break in order to return to hbmenu


        // Console crap
        PrintToScreen(GFX_TOP, frame % 400, 50, "Frame #%d", frame);
        if(frame++ > 60)
        {
            printf("Test #%u!\n", nTestCount++);
            frame = 0;
        }

        PrintToScreen(GFX_TOP, frame++ % 10, 10, "Size: %i", GetConsoleSize());
        FlushConsole();
        /////////////////////////////////////////////////////////////////////////////

		// Flush and swap framebuffers
		gfxFlushBuffers();
		gfxSwapBuffers();
	}

	CloseConsole();

	// Exit services
	gfxExit();
	hidExit();
	aptExit();
	srvExit();
	return 0;
}
