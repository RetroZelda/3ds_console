#include "Console.h"

// good luck reading.  thanks @smealum :)
#include "text.h"
#include "font.h"
#include <stdarg.h>

// internal list node
typedef struct _ConsoleStringNode
{
    char* _String;
    struct _ConsoleStringNode* _pNext;

} ConsoleStringNode;

// list-related
u32 _NumNodes = 0;
ConsoleStringNode* _pHead = NULL;

// settings-like stuff
gfxScreen_t _Screen;
u16 _ScreenWidth;
u16 _ScreenHeight;


// list functions
void AddFront(char* szText)
{
    if(!szText) return;
    ConsoleStringNode* pNewNode = (ConsoleStringNode*)malloc(sizeof(ConsoleStringNode));
    pNewNode->_String = szText;
    pNewNode->_pNext = NULL;

    AddNodeFront(pNewNode);
}

void AddNodeFront(ConsoleStringNode* pNode)
{
    if(!pNode) return;
    pNode->_pNext = _pHead;
    _pHead = pNode;
    _NumNodes++;
}

void RemoveNode(ConsoleStringNode* pNode)
{
    if(!pNode) return;
    ConsoleStringNode* pCurNode = _pHead;
    ConsoleStringNode* pPrevNode = NULL;
    while(pCurNode != NULL)
    {
        // check the string addr for a hit
        if(pNode->_String == pCurNode->_String)
        {
            // pull the node
            if(pPrevNode == NULL)
            {
                _pHead = pCurNode->_pNext;
            }
            else
            {
                pPrevNode->_pNext = pCurNode->_pNext;
            }

            // release it
            free(pCurNode->_String);
            free(pCurNode);
            _NumNodes--;
            return;
        }
        pPrevNode = pCurNode;
        pCurNode = pCurNode->_pNext;
    }
    // node isnt found here.  dont think asserts work.
}

// public functions
void OpenConsole(gfxScreen_t screenForConsole)
{
    _Screen = screenForConsole;
    _NumNodes = 0;
}


void CloseConsole()
{
    while(_pHead)
    {
        RemoveNode(_pHead);
    }
}

void PrintToConsole(char* szString, ...)
{
    // pass the args to sprintf
    va_list arguments;
    va_start ( arguments, szString );

    char* newString = calloc(CONSOLE_STRING_BUFFER_SIZE, 1);
    sprintf(newString, szString, arguments);

    va_end ( arguments );

    // TODO: Split multiple lines up
    AddFront(newString);
}

// print to screen this frame.  NOTE: allocates and frees memory
void PrintToScreen(gfxScreen_t screen, u16 X, u16 Y, char* szString, ...)
{
    va_list arguments;
    va_start ( arguments, szString );

    char* newString = calloc(CONSOLE_STRING_BUFFER_SIZE, 1);
    sprintf(newString, szString, arguments);

    va_end ( arguments );

    gfxDrawText(screen, GFX_LEFT, NULL, newString, Y, X);
    free(newString);
}

// draw all the text to the screen
void FlushConsole()
{
    // pull the currect active frame buffer
	u8* frameBuffer = gfxGetFramebuffer(_Screen, GFX_LEFT, &_ScreenWidth, &_ScreenHeight);

    // loop through every string in the dynarray
    ConsoleStringNode *pCurNode = _pHead;
    s16 sCount = 0;
    s16 sPosX = (fontDefault.height);
    s16 sPosY = 0;
	while(pCurNode)
    {
        sPosY = sCount++ * fontDefault.height;

        // check if the text should be removed
        if(sPosY >= (_ScreenHeight - fontDefault.height))
        {
            break;
        }
        else
        {
            gfxDrawText(_Screen, GFX_LEFT, NULL, pCurNode->_String, sPosY, sPosX);
        }
        pCurNode = pCurNode->_pNext;
    }

    // handle removing dead nodes
    if(pCurNode)
    {
        ConsoleStringNode *pCacheNode = pCurNode;
        while(pCurNode)
        {
            pCurNode = pCurNode->_pNext;
            RemoveNode(pCurNode);
        }
        RemoveNode(pCacheNode);
    }
}

u32 GetConsoleSize()
{
    return _NumNodes;
}

