#ifndef _CONSOLE_H_
#define _CONSOLE_H_

#include <stdlib.h>
#include <stdio.h>
#include <3ds.h>

#define CONSOLE_STRING_BUFFER_SIZE 256
#define printf(...) PrintToConsole(__VA_ARGS__)

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

void OpenConsole(gfxScreen_t screenForConsole);
void CloseConsole();
void PrintToConsole(char* szString, ...);
void PrintToScreen(gfxScreen_t screen, u16 X, u16 Y,char* szString, ...);
void FlushConsole();
u32 GetConsoleSize();

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // _CONSOLE_H_
